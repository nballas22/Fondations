using UnityEngine;
using System.Collections.Generic;

public class FSMState : Object
{
    #region Members

    protected object m_Reference;
    protected FSM m_Fsm;
    protected List<FSM> m_SubFsms = new List<FSM>();

    protected bool m_IsOver = false;

    #endregion


    #region Cst

    public FSMState(object reference)
    {
        m_Reference = reference;
    }

    #endregion


    #region Public Getters and Setters

    public bool IsOver
    {
        get
        {
            return m_IsOver;
        }
    }

    public object Reference
    {
        get
        {
            return m_Reference;
        }
    }

    public FSM Fsm
    {
        get
        {
            return m_Fsm;
        }
        set
        {
            m_Fsm = value;
        }
    }

    #endregion


    #region Enter Manipulators

    public virtual void Enter()
    {

    }

    public virtual bool CanEnter()
    {
        return true;
    }

    #endregion


    #region Exit Manipulators

    public virtual void Close()
    {
        m_IsOver = true;
    }

    public virtual bool CanExit()
    {
        return true;
    }

    public virtual void AskToExit()
    {
        Close();
    }

    public virtual void Exit()
    {

    }

    #endregion


    #region Virtual Manipulators

    public virtual void FrontToBack()
    {

    }

    public virtual void BackToFront()
    {

    }

    public virtual void FindComponent()
    {

    }

    #endregion
}
