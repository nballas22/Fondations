using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMHolder : MonoBehaviour
{
    #region Cst

    static FSMHolder s_Instance;
    public static FSMHolder Instance
    {
        get
        {
            // create FSMHolder
            if (s_Instance == null)
                s_Instance = new GameObject("FSMHolder").AddComponent<FSMHolder>();

            return s_Instance;
        }
    }

    #endregion


    #region Static Manipulators

    public static void AddFSM(FSM fsm)
    {
        Instance.AddFSM_Internal(fsm);
    }

    #endregion


    #region Private Manipulators

    void AddFSM_Internal(FSM fsm)
    {
        FSM newFsm = s_Instance.gameObject.AddComponent<FSM>();
        newFsm = fsm;
    }

    #endregion
}
