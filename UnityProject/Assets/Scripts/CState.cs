public class CState
{
    public FSMState State;
    public EQueue Queue;
    public EStack Stack;
    public EExit Exit;

    public CState(FSMState state, EQueue queue, EStack stack, EExit exit)
    {
        State = state;
        Queue = queue;
        Stack = stack;
        Exit = exit;
    }
}
