using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour
{
    #region Members

    object m_Reference;
    Stack<CState> m_StateStack = new Stack<CState>();
    Queue<CState> m_Queue = new Queue<CState>();
    FSMState m_TopState;
    Dictionary<object, FSMState> m_StateDictionnary = new Dictionary<object, FSMState>();

    #endregion


    #region Cst

    public FSM(object Reference)
    {
        m_Reference = Reference;
    }

    #endregion


    #region Runtime Manipulators

    void Update()
    {
        ProceedExit();
        ProceedQueue();
    }

    /// <summary>
    /// Recursion to stop all states that need to be stopped
    /// </summary>
    void ProceedExit()
    {
        if (m_StateStack.Count > 0)
        {
            if (m_TopState.IsOver)
            {
                if (m_TopState.CanExit())
                {
                    StopTopState();
                    ProceedExit();
                }
            }
        }
    }

    void StopTopState()
    {
        // get top state
        CState cState = m_StateStack.Pop();
        FSMState state = cState.State;
        if (state == null)
            return;

        // stop top state
        state.Exit();

        // set 2nd state to front
        m_TopState = m_StateStack.Peek().State;
        m_TopState.BackToFront();
    }

    /// <summary>
    /// Recursion to start all states that need to be started
    /// </summary>
    void ProceedQueue()
    {
        if (m_Queue.Count > 0)
        {
            CState cState = m_Queue.Peek();
            if (cState.State.CanEnter())
            {
                StartNextState();
                ProceedQueue();
            }
        }
    }

    void StartNextState()
    {
        // get next state
        CState cState = m_Queue.Dequeue();
        FSMState state = cState.State;
        if (state == null)
            return;

        // set top state in background 
        if (m_StateStack.Count > 0)
            m_TopState.FrontToBack();

        // start next state
        m_StateStack.Push(cState);
        state.Enter();
        m_TopState = state;
    }

    void AddStateToQueue(CState cState)
    {
        switch (cState.Queue)
        {
            case EQueue.Clear:
                m_Queue.Clear();
                break;
            default:
                break;
        }

        m_Queue.Enqueue(cState);
    }

    #endregion


    #region Public Manipulators

    public void RegisterState(FSMState state)
    {
        if (m_StateDictionnary == null)
            m_StateDictionnary = new Dictionary<object, FSMState>();

        m_StateDictionnary.Add(state.Reference, state);
        state.Fsm = this;
    }

    public void Unload()
    {

    }

    public void UnloadWithFade()
    {
        Unload();
    }

    public void SetState(object reference, EQueue queue = EQueue.Push, EStack stack = EStack.Push, EExit exit = EExit.DoNothing)
    {
        Debug.Log("SetState: " + reference);
        if (reference == null)
        {
            Debug.LogError("Tried to set a state with a null reference");
            return;
        }

        FSMState state = m_StateDictionnary[reference];
        if (state == null)
        {
            Debug.LogError("Tried to set an inexisting state. Reference: " + reference.ToString());
            return;
        }

        AddStateToQueue(new CState(state, queue, stack, exit));
    }

    #endregion


    #region Log Manipulators

    public void LogStack(bool showParameters)
    {
        string res = string.Format("LogStack : {0} states active in {1}\n", m_StateStack.Count, m_Reference);
        foreach (CState cState in m_StateStack)
        {
            if (showParameters)
                res += string.Format("> {0} - Queue: {1}, Stack: {2}, Exit: {3}\n", cState.State.Reference, cState.Queue, cState.Stack, cState.Exit);
            else
                res += string.Format("> {0}\n", cState.State.Reference);
        }
        Debug.Log(res);
    }

    public void LogQueue(bool showParameters)
    {
        string res = string.Format("LogQueue : {0} states in queue of {1}\n", m_StateStack.Count, m_Reference);
        foreach (CState cState in m_Queue)
        {
            if (showParameters)
                res += string.Format("> {0} - Queue: {1}, Stack: {2}, Exit: {3}\n", cState.State.Reference, cState.Queue, cState.Stack, cState.Exit);
            else
                res += string.Format("> {0}\n", cState.State.Reference);
        }
        Debug.Log(res);
    }

    #endregion


    #region Virtual Manipulators

    public virtual void Enter()
    {

    }

    #endregion
}
