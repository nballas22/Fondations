using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeState : MenuState
{
    #region Members


    #endregion


    #region Cst

    public HomeState() : base(EMenuState.Home)
    {

    }

    #endregion


    #region Inherited Manipulators

    public override void Enter()
    {
        Debug.Log("Enter in HomeState");

        base.Enter();
    }

    #endregion
}
