using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    [HideInInspector]
    public FSM ApplicationFsm;

    static Transform s_CoreSystem;

    void Awake()
    {
        s_CoreSystem = transform;
        CreateAppFsm();
    }

    void CreateAppFsm()
    {
        Debug.Log("CreateSubFsm");
        FSM fsm = gameObject.AddComponent<MainMenuFsm>();
        ApplicationFsm = fsm;

        fsm.RegisterState(new HomeState());
        fsm.RegisterState(new Menu1State());
        fsm.RegisterState(new Menu2State());

        fsm.Enter();

    }

    public static Transform CoreSystem
    {
        get
        {
            return s_CoreSystem;
        }
    }
}
