using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuFsm : FSM
{
    #region Cst

    public MainMenuFsm() : base(EFsm.MainMenu)
    {

    }

    #endregion


    #region Inherited Manipulators

    public override void Enter()
    {
        base.Enter();
        SetState(EMenuState.Home);
    }

    #endregion
}
