public enum EStack
{
    //
    // R�sum�:
    //     Ask all previous states to exit ASAP then push as top state (they will not necessary
    //     exit instantly though).
    Clear = 0,
    //
    // R�sum�:
    //     Ask top state only to exit ASAP then push as top state (they will not necessary
    //     exit instantly though).
    Replace = 1,
    //
    // R�sum�:
    //     Keeps previous states active then push as top state
    Push = 2
}

public enum EQueue
{
    //
    // R�sum�:
    //     Clears next states list at SetState time.
    Clear = 0,
    //
    // R�sum�:
    //     Waits for queue to be ready to start this state
    //     Prechains logic using this functionality.
    Push = 1
}

public enum EExit
{
    DoNothing = 0,
    Unregister = 1
}

public enum EFsm
{
    MainMenu
}

public enum EAppState
{
    Main
}

public enum EMenuState
{
    Home,
    Menu1,
    Menu2,
}

