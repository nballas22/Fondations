using UnityEngine;

public class AssetLoader
{
    public static Object LoadAsset(string path)
    {
        return Resources.Load(path);
    }
}
