using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuState : FSMState
{
    #region Members

    string m_PrefabPath = "Prefabs/Screen/Menu/";

    protected GameObject m_Prefab;

    #endregion


    #region Cst

    public MenuState(object reference) : base(reference)
    {

    }

    #endregion


    #region Inherited Manipulators

    public override void Enter()
    {
        base.Enter();

        LoadPrefab(m_PrefabPath + m_Reference.ToString());
    }

    #endregion


    #region Private Manipulators

    void LoadPrefab(string prefabPath)
    {
        m_Prefab = AssetLoader.LoadAsset(prefabPath) as GameObject;
        Instantiate(m_Prefab, Main.CoreSystem);
        if (m_Prefab)
            OnPrefabLoaded();
    }

    #endregion


    #region Virtual Manipulators

    protected virtual void OnPrefabLoaded()
    {

    }

    #endregion
}
